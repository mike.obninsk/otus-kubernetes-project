#! /bin/bash
#https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-reindex.html#reindex-from-remote
set -o errexit
OLD_ES='10.110.63.47:9200'
OLD_ES_SCHEME='https'
OLD_ES_AUTH='-u elastic:mSSZ6epX2TQqGE4W'
NEW_ELASTIC='10.97.54.199:9200'
NEW_ES_SCHEME='https'
NEW_ES_AUTH='-u elastic:MWtBKGDF2qnKr889'
INDEX_TMPLT='sblesb'

INDEXES=`bash -c "curl -k $OLD_ES_AUTH $OLD_ES_SCHEME://$OLD_ES/_aliases?pretty=true 2>&1 | grep $INDEX_TMPLT | cut -d ' ' -f 3"`

for INDEX in $INDEXES; do
echo $INDEX
curl -k $NEW_ES_AUTH -XPOST -H "Content-Type: application/json" $NEW_ES_SCHEME://$NEW_ELASTIC/_reindex?pretty -d'{
  "source": {
    "remote": {
      "host": "'$OLD_ES_SCHEME'://'$OLD_ES'",
      "username": "elastic",
      "password": "password"
    },
    "index": '$INDEX',
    "size": 10
  },
  "dest": {
    "index": '$INDEX'
  }
}'
done
