# Установка ELK

## Установка eck operator
```bash
kubectl apply -f ./crds_/all-in-one_1.2.1.yaml
```
По умолчанию оператор устанавливается в namespace "elastic-system"

## Cоздание namespace
Namespace должен называться **elk**, поскольку в дальнейшем это имя также используется в непараметризуемых константах в чартах.
```bash
kubectl create namespace elk
```

## Установка релиза с помощью чарта в конфигурации dev-среды
Релиз **ElasticSearch** и **Kibana** тоже должен называться **elk**, поскольку в дальнейшем это имя также используется в непараметризуемых константах в чартах (в частности в logstash и nginx).
```
helm upgrade --install elk --namespace elk ./charts/elk/elasticsearch/ --debug -f ./charts/elk/elasticsearch/values-dev.yaml --timeout=1200s --dry-run
```

## Установка Logstash
Logstash устанавливается с помощью отдельного чарта в неймспейс **elk**:
```
helm upgrade --install -n elk logstash ./charts/elk/logstash/ --debug -f ./charts/elk/logstash/values-dev.yaml --timeout=1200s --dry-run
```
