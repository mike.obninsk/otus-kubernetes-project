# Подготовка хоста
Все манипуляции я выполнял на Ubuntu 20.04.

## Terraform
### Установка terraform
```
wget https://releases.hashicorp.com/terraform/0.13.1/terraform_0.13.1_linux_amd64.zip
unzip ./terraform_0.13.1_linux_amd64.zip 
sudo mv ./terraform /usr/local/bin/terraform
```

### Установска модулей terraform
```
terraform init
```

## Установка gcloud
```
echo "deb https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get -y upgrade && sudo apt-get -y install google-cloud-sdk kubectl
```
инициализация:
```
gcloud init
```

### Получение токена (access_token)
В дальнейшем полученное значение используется для доступа terraform к gcloud. \
```
gcloud auth print-access-token
```
### Включаем сервисы на GCP
```
gcloud services enable compute.googleapis.com
gcloud services enable servicenetworking.googleapis.com
```

## Ansible
### Установка ansible и модулей python
```
sudo pip install ansible netaddr
```

# Разворачивание
Без piplining кластер из шести нод (3 мастера и 3 воркера) разворачивался 1 час 17 минут. \
Без piplining кластер из шести нод (3 мастера и 3 воркера) разворачивался 29m57s . \
C piplining кластер из двух нод (1 мастер и 1 воркер) разворачивался 16m47s. \


# OpenEBS Dynamic Local PV provisioner
Для автоматического провижининга PV на HostPath я попробывал применить **OpenEBS Dynamic Local PV provisioner**. Он позволяет указать директорию, в которй будут автоматически (при создании PVC) создаваться PV типа HostPath. Позволяет делать бекапы с помощью Velero (не знаю что это). \
https://docs.openebs.io/docs/next/uglocalpv-hostpath.html \
``` 
kubectl apply -f https://openebs.github.io/charts/openebs-operator.yaml
```

StorageClass
```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: local-hostpath
  annotations:
    openebs.io/cas-type: local
    cas.openebs.io/config: |
      - name: StorageType
        value: hostpath
      - name: BasePath
        value: /var/lib/data/
provisioner: openebs.io/local
reclaimPolicy: Retain
volumeBindingMode: WaitForFirstConsumer
```

PVC:
```
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: local-hostpath-pvc
spec:
  storageClassName: local-hostpath
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5G
```

Столкнулся вот с такой ошибкой: https://github.com/openebs/openebs/issues/3046
Пока непонятно как ее обходить...Вот тут предположили, что причина в DNS. https://stackoverflow.com/questions/63186799/cannot-create-a-pvc-after-following-openebs-installation-instructions-on-a-bare \
В качестве временной меры - можно просто отключить этот admission. https://github.com/openebs/openebs/issues/3046 \
Для этого - нужно пропатчить конфиг:
```
kubectl patch validatingwebhookconfigurations  openebs-validation-webhook-cfg --type=json -p='[{"op": "replace", "path": "/webhooks/0/failurePolicy", "value":"Ignore"}]'
```
То есть установить значение failurePolicy в Ignore \