#!/bin/bash
export KUBECONFIG=./kubeconfig

kubectl create ns elk
kubectl apply -f https://download.elastic.co/downloads/eck/1.2.1/all-in-one.yaml
helm upgrade --install elk -n elk ../infra_apps/elk/elasticsearch
helm upgrade --install logstash -n elk ../infra_apps/elk/logstash


## Add repos to helm
helm repo add gitlab https://charts.gitlab.io
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

## Install GitLab Runner
kubectl create ns gitlab
helm upgrade --install --namespace gitlab gitlab-runner -f ./gitlab-runner-values.yaml gitlab/gitlab-runner
kubectl apply -f - <<EOF
apiVersion: rbac.authorization.k8s.io/v1
kind: "ClusterRoleBinding"
metadata:
  name: gitlab-runner-admin
  namespace: gitlab
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: "ClusterRole"
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: default
  namespace: gitlab
EOF


# Install Prometheus
# https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus
kubectl create ns monitoring
helm upgrade --install prometheus -n monitoring prometheus-community/prometheus


## Install Docker Registry
kubectl create ns docker
helm upgrade --install -n docker registry stable/docker-registry