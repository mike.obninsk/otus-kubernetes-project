terraform {
  required_providers {
    external = {
      source = "hashicorp/external"
    }
    gitlab = {
      source = "terraform-providers/gitlab"
    }
    google-beta = {
      source = "hashicorp/google-beta"
    }
    null = {
      source = "hashicorp/null"
    }
  }
  required_version = ">= 0.13"
}
