#!/bin/bash
set -o nounset
set +e

echo "Disabling shared runners..."
# Disable shared Runners
curl --fail --silent --header "PRIVATE-TOKEN: bYX8XgJTc2xZWcpN29cN" --request PUT "https://gitlab.com/api/v4/projects/21091944?shared_runners_enabled=no" &>/dev/null && echo "Done!"

# Get Runners
runnerIds=$(curl --fail --silent --header "PRIVATE-TOKEN: bYX8XgJTc2xZWcpN29cN" "https://gitlab.com/api/v4/projects/21091944/runners?per_page=10000000000000000000000" | jq '.[].id' | tr '\n' ' ')

#offlineRunnerIds=$(curl --fail --silent --header "PRIVATE-TOKEN: bYX8XgJTc2xZWcpN29cN" "https://gitlab.com/api/v4/runners?per_page=10000000000000000000000" | jq '.[].id' | tr '\n' ' ')
#onlineRunnerIds=$(curl --fail --silent --header "PRIVATE-TOKEN: bYX8XgJTc2xZWcpN29cN" "https://gitlab.com/api/v4/runners/all?scope=online&per_page=10000000000000000000000" | jq '.[].id' | tr '\n' ' ')

echo "Removing existing runners..."
# Remove runners
for id in $runnerIds; do
    curl --fail --silent --request DELETE --header "PRIVATE-TOKEN: bYX8XgJTc2xZWcpN29cN" "https://gitlab.com/api/v4/runners/$id"
done

echo "Done!"