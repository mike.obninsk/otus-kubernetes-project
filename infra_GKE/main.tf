# This is the provider used to spin up the gcloud instance
provider "google-beta" {
  project = var.project_name
  region  = var.region_name
  zone    = var.zone_name
  access_token = data.google_client_config.default.access_token
  #data.external.access_token.result.token
}

resource "google_container_cluster" "gke-cluster" {
  provider = google-beta
  name     = var.gke_cluster_name
  location = var.zone_name

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  addons_config {
    istio_config  {
        disabled = false
    }
  }
}

resource "google_container_node_pool" "gke-cluster-nodes" {
  provider = google-beta
  name       = "${var.gke_cluster_name}-node-pool"
  location   = var.zone_name
  cluster    = google_container_cluster.gke-cluster.name
  node_count = var.cluster_node_count

  node_config {
    preemptible  = true
    machine_type = var.machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

data "google_client_config" "default" {
}

data "google_container_cluster" "gke-cluster" {
  provider = google-beta
  name = google_container_cluster.gke-cluster.name
  zone = google_container_cluster.gke-cluster.location
}

provider "gitlab" {
    token = var.gitlab_token
    base_url = "${var.gitlab_url}/api/v4/"
}

data "gitlab_project" "gitlab_project" {
    id = var.gitlab_project_id
}

resource gitlab_project_cluster "gke" {
  project                       = var.gitlab_project_id
  name                          = google_container_cluster.gke-cluster.name
  domain                        = var.cluster_domain
  enabled                       = true
  kubernetes_api_url            = "https://${google_container_cluster.gke-cluster.endpoint}"
  kubernetes_token              =  data.google_client_config.default.access_token
  #data.google_client_config.default.access_token
  kubernetes_ca_cert            = base64decode(
    google_container_cluster.gke-cluster.master_auth[0].cluster_ca_certificate,
  )
  #kubernetes_namespace          = "namespace"
  kubernetes_authorization_type = "rbac"
  #environment_scope             = "*"
  #management_cluster_id         = "123456"
  managed = false
}

resource "gitlab_project_variable" "docker_registry_login" {
   project   = var.gitlab_project_id
   key       = "DOCKER_REGISTRY_LOGIN"
   value     = var.docker_registry_login
   protected = false
   environment_scope = "*"
}

resource "gitlab_project_variable" "docker_registry_pass" {
   project   = var.gitlab_project_id
   key       = "DOCKER_REGISTRY_PASS"
   value     = var.docker_registry_pass
   protected = false
   environment_scope = "*"
}

resource "gitlab_project_variable" "docker_registry_url" {
   project   = var.gitlab_project_id
   key       = "DOCKER_REGISTRY_URL"
   value     = var.docker_registry_url
   protected = false
   environment_scope = "*"
}

resource "gitlab_project_variable" "docker_repo_name" {
   project   = var.gitlab_project_id
   key       = "DOCKER_REPO_NAME"
   value     = var.docker_repo_name
   protected = false
   environment_scope = "*"
}

# Получаем KUBECONFIG для доступа к кластеру.
resource "null_resource" "get_kubeconfig" {
  depends_on  =   [google_container_cluster.gke-cluster]

  provisioner "local-exec" {
    command     = "KUBECONFIG=./kubeconfig gcloud container clusters get-credentials ${var.gke_cluster_name} --zone ${var.zone_name} --project ${var.project_name}"
    working_dir = path.module
  }
}

resource "null_resource" "install_infra_apps" {
  depends_on  =   [null_resource.get_kubeconfig, local_file.runner-values, null_resource.cleanup_runners]

  provisioner "local-exec" {
    command     = "./install_infra_apps.sh"
    working_dir = path.module
  }
}

resource local_file "runner-values" {
  filename = var.gitlab_runner_values_file
  content  = templatefile (var.gitlab_runner_values_file_template, { gitlab_url = var.gitlab_url,
                            gitlab_runnerRegistrationToken = data.gitlab_project.gitlab_project.runners_token })
}

resource local_file "gitlab-runner-cleanup-script" {
  filename = var.gitlab_runners_cleanup_script
  content  = templatefile (var.gitlab_runners_cleanup_script_template, { gitlab_url = var.gitlab_url,
                            gitlab_token = var.gitlab_token,
                            gitlab_project_id = var.gitlab_project_id })
}

resource "null_resource" "cleanup_runners" {
  depends_on  =  [local_file.gitlab-runner-cleanup-script]

  provisioner "local-exec" {
    command     = "sh ./${var.gitlab_runners_cleanup_script}"
    working_dir = path.module
  }
}