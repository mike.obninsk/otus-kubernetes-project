# Что это
Это курсовой проект по курсу "OTUS - Инфраструктурная платформа на основе Kubernetes"

# Что сделано сейчас
  * Реализовано разворачивание кластера с помощью terraform. В двух вариантах - baremetal (GCP + kubespray) и managed (GKE). Дальше буду развивать вариант с  GKE, потому что там несколько проще развернуть сопутствующие приложения (ELK, prometheus).
  * Для варианта baremetal (GCP + kubespray) частично реализовано разворачивание прикладной инфраструктуры (ELK, в процессе prometheus).
  * Код terraform подключает созданный кластер GKE к проекту GitLab. 

# Над чем работаю прямо сейчас
  * Автоматизирую разворачивание ELK и prometheus (Managed Applications) - https://docs.gitlab.com/ee/user/clusters/applications.html
  * Бодаюсь с CI



# Подготовка хоста
Все манипуляции я выполнял на Ubuntu 20.04.

## Terraform
### Установка terraform
```
wget https://releases.hashicorp.com/terraform/0.13.1/terraform_0.13.1_linux_amd64.zip
unzip ./terraform_0.13.1_linux_amd64.zip 
sudo mv ./terraform /usr/local/bin/terraform
```

### Установска модулей terraform
```
terraform init
```

## Установка gcloud
```
echo "deb https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get -y upgrade && sudo apt-get -y install google-cloud-sdk kubectl
```
инициализация:
```
gcloud init
```

### Получение токена (access_token)
Для того, чтобы можно было использовать дефолтную аутентификацию gcloud и конструкции вида:
```
data "google_client_config" "default" {
}
...
provider "google-beta" {
  ...
  access_token = data.google_client_config.default.access_token
}
``` 
выполняем
```
gcloud auth application-default login
```

### Включаем сервисы на GCP
```
gcloud services enable compute.googleapis.com
gcloud services enable servicenetworking.googleapis.com
```

## Ansible
### Установка ansible и модулей python
```
sudo pip install ansible netaddr
```

## GitLab
На GitLab надо получить пару токенов и ID проекта и прописать их в качестве значений переменных в variables.auto.tfvars:
  * **gitlab_token** - для доступа к API (Settings -> Access Tokens -> )
  * **gitlab_runnerRegistrationToken**для регистрации runner'а (Project Settings -> CI/CD -> Runners -> Set up a specific Runner manually)
  * **gitlab_project_id**

## Docker Registry
Нужно создазть registry, куда мы будем кидать собранные образы и токен для доступа к нему. \\
Их нужно прописать в соотвествующие переменные.
